# trends
Toot trending now hashtags to your favorite Mastodon server

### Dependencies

-   **Python 3**
-   Postgresql server
-   Everything else at the top of `trends.py`!

### Usage:

Within Python Virtual Environment:

1. Run 'python setup.py' to get your bot's access token of a Mastodon existing account. It will be saved to 'secrets/secrets.txt' for further use.

2. Run 'python trends.py' to start tooting trending now hashtags.

3. Use your favourite scheduling method to set trends.py to run regularly. 

Note: install all needed packages with 'pip install package' or use 'pip install -r requirements.txt' to install them. 


