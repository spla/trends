#!/usr/bin/env python
# -*- coding: utf-8 -*-

from six.moves import urllib
from datetime import datetime, timedelta
from mastodon import Mastodon
import time
import re
import os
import json
import time
import signal
import sys
import os.path        # For checking whether secrets file exists
import requests       # For doing the web stuff, dummy!
import operator
import calendar

def take_third(elem):
    return elem[2]

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath) # E.g., mastodon.social

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + mastodon_hostname,
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

#############################################################

init_rem_requests = mastodon.ratelimit_remaining
print("Remaining API requests: " + str(init_rem_requests))

next_reset = mastodon.ratelimit_reset
next_reset = datetime.fromtimestamp(next_reset)
next_reset = next_reset.strftime("%d/%m/%Y, %H:%M:%S")
print("Next API reset: " + str(next_reset))

#########################################################

res = requests.get('https://' + mastodon_hostname + '/api/v1/trends')

count = len(res.json())
trends = []

i = 0
while i < count:

  trends.append(res.json()[i])
  i += 1

trend_url = []
trend_name = []
trend_uses = []
trend_users = []

i = 0
while i < len(trends):

  if int(trends[i]['history'][0]['uses']) > 5:

    trend_url.append(trends[i]['url'])
    trend_name.append(trends[i]['name'])
    trend_uses.append(int(trends[i]['history'][0]['uses']))
    trend_users.append(int(trends[i]['history'][0]['accounts']))

  i += 1

toot_text = ""

#################################################################
# order feeds
#################################################################

all_feeds_temp = zip(trend_url, trend_name, trend_uses, trend_users)
all_feeds = set(all_feeds_temp)
all_feeds_list = list(all_feeds)
sorted_feeds = sorted(all_feeds_list, key = take_third, reverse = True)

#################################################################

i = 0
while i < len(sorted_feeds):

  toot_text += "\n"
  toot_text += "#" + str(sorted_feeds[i][1]) + "\n"
  toot_text += str(sorted_feeds[i][2]) + " users, " + str(sorted_feeds[i][3]) + " times" + "\n"
  if len(toot_text)> 400:
    break

  i += 1

if toot_text != "":
  print(toot_text)
  mastodon.status_post(toot_text, in_reply_to_id=None, )
